## Argocd Stuff

### all-argocd-applications
![all-argocd-applications](docs/images/all-argocd-applications.png)

### argocd-application-aws-infra
![argocd-application-aws-infra](docs/images/argocd-application-aws-infra.png)

### argocd-application-gcp-infra
![argocd-application-gcp-infra](docs/images/argocd-application-gcp-infra.png)

## AWS Infra Stuff

### EKS-cluster
![eks-cluster](docs/images/eks-cluster.png)

## GCP Infra Stuff

### GKE-cluster
![gke-cluster](docs/images/gke-cluster.png)
