#!/bin/bash

# Check if kind command is available
if ! command -v kind &> /dev/null; then
    echo "Kind is not installed. Installing..."
    # Download and install Kind
    [ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.22.0/kind-$(uname)-amd64
    chmod +x ./kind
    sudo mv ./kind /usr/local/bin/kind
    echo "Kind installed successfully."
else
    echo "Kind is already installed."
fi

# Define the cluster name
cluster_name="my-kind-cluster"

# Check if the cluster already exists
if kind get clusters | grep -q $cluster_name; then
    echo "Cluster $cluster_name already exists. Skipping creation."
else
    # Create a Kind cluster
    kind create cluster --name $cluster_name
fi